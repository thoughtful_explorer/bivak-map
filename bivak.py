#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import simplekml
import re

#Set filename/path for KML file output
kmlfile = "bivak.kml"
#Set KML schema name
kmlschemaname = "bivak"
#Set page URL
pageURL = "https://bivakzone.be"

#Returns reusable BeautifulSoup of the site, defaulting to the locations list page
def getsoupinit(url=pageURL+"/overzichtskaart.html"):
    #Start a session with the given page URL
    session = requests.Session()
    page = session.get(url)
    #Return soupified HTML
    return BeautifulSoup(page.content, 'html.parser')

#Returns a list of all sites
def getsites():
    #Initialize the soup as a list of all ul's of the art-vmenu class
    soup = getsoupinit()(class_="art-vmenu")
    #Initialize a list to hold links
    links =[]
    #Loop through each ul...
    for ul in soup:
        #...and within each ul, through each li...
        for li in ul:
            #...and append each of those links within the list items to the links list
            links.append(pageURL+li.a["href"])
    return links

#Given a table information from a Bivak site page, and whether the page contains multiple sites or not; return the coordinates lat and lng
def getcoords(row,multi=False):
    if multi is True:
        #Create a list of the tds within this row
        tds = row.find_all("td")
        #Choose the 4th td, as this contains the WGS84 coordinates
        td = tds[3]
        #Again, this html is a mess, so do it one way if we find an id attribute on the second span tag...
        if td.span.span.has_attr("id"):
        #Push the latitude, stripping the website's ever-present junk whitespace
            lat = td.select("span#lat_dec")[0].get_text().strip()
            lng = td.select("span#long_dec")[0].get_text().strip()
        else:
            #...otherwise, there is no second span tag and we have to retrive the coordinates another way, with a split based on \xa0 (AKA <br/>)
            latlng = td.span.span.get_text().split("\xa0")
            #Assign the coordinates to their appropriate variables, stripping off the website's ever-present junk whitespace
            lat = latlng[0].strip()
            #This html is a mess. The table row for Hooilingen in https://bivakzone.be/5-x-haspengouw.html is formatted differently from the others.
            if lat == "50.843167":
                latlng = td.span.span.get_text().split("\xa0\xa0")
                lng = latlng[1].strip()
            else:
                lng = latlng[1].strip()
    else:
        #Create a list of the tds within this row
        tds = row.find_all("td")
        #Choose the 2nd td, as this contains the WGS84 coordinates
        td = tds[1]
        #Split the td's outer span into its constituent spans
        #spans = td.span.find_all("span")
        #Assign the coordinates from the text values of their respective spans, stripping off the website's ever-present junk whitespace
        latlng = td.span.get_text().split(" ")
        if latlng[0] =="":
            latlng = td.span.get_text().split("\xa0")
        #Filtering out more html garbage
        if len(latlng) == 1:
            latlng = latlng[0].split("\xa0")
        lat = latlng[0]
        lng = latlng[1]
        #Another fix for this garbage html on the https://bivakzone.be/arcadia-muizen.html page - major decimal number is outside the apporiate tag
        if lat == "51.0035":
           lng = 4.542833
    return lat,lng

#Returns site information for a given url
def getsite(url):
    #Initialize the soup
    soup = getsoupinit(url)
    #Initialize site list because sometimes we must return more than one site in this function
    sites=[]
    #print(url)    
    #If the title of the page has a number in it, this is a webpage that contains a table of multiple sites and we need to search the soup in a special way... 
    if re.search("\d", soup.title.get_text()):
        #Additionally, if the page is Duinengordel, we need to do more special things
        if url == "https://bivakzone.be/duinengordel.html":
            #Get the relevant tables from the page (excludes the first of three)
            tables = soup.find_all("table")[1:]
            for table in tables:
                #Assign the name from the first td and strip whitespace (this site's html is a big mess), while adding Bivakzone prefix to reduce vagueness if presented among other map layers
                name = "Bivakzone "+table.h4.span.strong.get_text().strip()
                #Isolate the 7th tds for coordinate data
                tds = table.find_all("td")[6]
                #Split the span values by \xa0 (AKA <br/>) to isolate lat and lng
                latlong = tds.span.get_text().split("\xa0")
                #Assign the coordinates
                lat = latlong[0]
                lng = latlong[1]
                sites.append([name,lat,lng])
        else:
            #Search through the whole page for occurrences of "WGS84" in <strong> tags and expand to the whole table - this is where the coordinate information is
            table = soup.find_all("strong",string=re.compile("WGS84"))[0].parent.parent.parent
            #Get all the table rows, shedding off the first and last
            tr = table.find_all("tr")[1:-1]
            for row in tr:
                #Assign the name from the first td and strip whitespace (this site's html is a big mess), while adding Bivakzone prefix to reduce vagueness if presented among other map layers
                name = "Bivakzone "+row.td.get_text().strip()
                #Assign lat and lng variables all at once with the getcoords function's multiple returned variables
                lat, lng = getcoords(row,multi=True)
                sites.append([name,lat,lng])
    #...otherwise, this is a webpage with information and a table regarding a single site
    else:
        #Get the name - normally this is already presented with "Bivakzone" prefix to reduce vagueness if presented among other map layers
        name = soup("title")[0].get_text()
        #Search through the whole page and collect occurrences of WGS84 in <strong> tags - these are always where the coordinate information is
        #for coords in soup.find_all("strong",string=re.compile("WGS84")):
        #    print(coords.parent.parent.parent)
        tr = soup("strong",string=re.compile("WGS84"))[0].parent.parent.parent
        lat, lng = getcoords(tr)
        sites.append([name,lat,lng])
    return sites

#Returns a list of all the sites
def getallsites():
    #Initialize sites list
    sites=[]
    #Loop through all the sites
    for site in getsites():
        #Use try to avoid erroring out on empty geo values, and add the site to the site list
        try:
            sites.extend(getsite(site))
	#...but if there is missing data, skip it because there are some pages which lack geographic information
        except IndexError:
            continue
    return sites

#Saves a KML file of a given list of stores
def createkml(sites):
    #Initialize kml object
    kml = simplekml.Kml()
    #Iterate through datamap for each site
    for site in sites:
        #Get the name of the site
        sitename = site[0]
        #Get coordinates of the site
        lat = site[1]
        lng = site[2]
        #First, create the point name and description in the kml
        point = kml.newpoint(name=sitename,description="camping")
        #Finally, add coordinates to the feature
        point.coords=[(lng,lat)]
    #Save the final KML file
    kml.save(kmlfile)
#Bring it all together
createkml(getallsites())
