# Bivak Map

Extracts Bivak camping locations from the Bivakzone website, and places as many of them as possible into KML format.

## Dependencies
* Python 3.x
    * Requests module for HTTP requests
    * Beautiful Soup 4.x (bs4) for scraping
    * Simplekml for easily building KML files
    * Regular expressions module for text matching
* Also of course depends on official [Bivakzone website](https://bivakzone.be).
